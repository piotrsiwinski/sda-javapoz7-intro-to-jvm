package pl.sda.poznan.fileoperation;

import java.util.List;

public class Program {

  public static void main(String[] args) {
    List<String> strings = FileHelper
        .readAllLines("C:\\Users\\siwipi\\java\\sda-javapoz7-intro-to-jvm\\mojplik.txt");


    String result = StringHelper.concatenateWithStringBuilder(strings);

    System.out.println(result);
  }
}
