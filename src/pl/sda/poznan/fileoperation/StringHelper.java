package pl.sda.poznan.fileoperation;

import java.util.List;

public class StringHelper {

  /**
   * Funkcja ktora przyjmuje liste i konkatenuje jej elementy do pojedynczego obiektu typu String
   */
  public static String concatenateAllStrings(List<String> list) {
    String result = "";
    for (String elem : list) {
      result = result + elem + "\n";
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    return result;
  }

  public static String concatenateWithStringBuilder(List<String> list) {
    StringBuilder builder = new StringBuilder();
    for (String elem : list) {
      builder.append(elem).append("\n");
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    return builder.toString();
  }


}
