package pl.sda.poznan;

import java.util.Scanner;
import pl.sda.poznan.helper.MathHelper;

public class Main {

    public static void main(String[] args) {

//        Scanner scanner = new Scanner(System.in);
        try {
            int factorial = MathHelper.factorial(-3);
        } catch (IllegalArgumentException ex) {
            String message = ex.getMessage();
            System.out.println("Cannot calculate. " + message);
        }
    }
}
